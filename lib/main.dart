import 'package:flutter/material.dart';
import './pages/map_ant.dart';
import './pages/course_selection.dart';
import './pages/home.dart';
import 'package:flutter/services.dart';
import 'package:wakelock/wakelock.dart';
import 'package:flex_color_scheme/flex_color_scheme.dart';
import 'package:flutter_map_tile_caching/flutter_map_tile_caching.dart';
import 'classes/localmaps.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await FlutterMapTileCaching.initialise();
  await FMTC.instance('mapStore').manage.createAsync();
  FMTC.instance('mapStore').download.startForeground(region: downloadable);

  // ...
  runApp(const MyApp());
}

//void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    Wakelock.enable();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    return MaterialApp(
        theme: FlexColorScheme.dark(
                scheme: FlexScheme.amber,
                visualDensity: FlexColorScheme.comfortablePlatformDensity)
            .toTheme,
        initialRoute: HomePage.route,
        routes: <String, WidgetBuilder>{
          HomePage.route: (context) => const HomePage(),
          //SignInDemo.route: (context) => SignInDemo(),
          CustomCrsPage.route: (context) => CustomCrsPage(),
          CourseSelection.route: (context) => const CourseSelection(),
        });
    // home: CustomCrsPage(),
    //routes: <String, WidgetBuilder>{
    //  CustomCrsPage.route: (context) => CustomCrsPage(),
    //  CourseSelection.route: (context) => CourseSelection(),
    //}
  }
}
