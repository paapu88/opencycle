import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong2/latlong.dart';

class Piste {
  var piste = <CircleMarker>[];

  Piste(double lat, double lon) {
    piste.add(CircleMarker(
      //radius marker
      point: LatLng(lat, lon),
      color: Colors.yellow.withOpacity(1.0),
      borderStrokeWidth: 3.0,
      //useRadiusInMeter: true,
      //radius: 50, //radius
      //useRadiusInMeter: true,
      radius: 5, //radius
      borderColor: Colors.red,
    ));
    //print(piste);
  }
}
