import 'package:flutter/widgets.dart';
import 'package:flutter_map_tile_caching/flutter_map_tile_caching.dart';
import 'package:geodesy/geodesy.dart';
import 'package:flutter_map/flutter_map.dart';

final region = CircleRegion(
  LatLng(48.32, 14.31), // Center
  100, // KM Radius
);

final downloadable = region.toDownloadable(
  10, // Minimum Zoom
  17, // Maximum Zoom
  TileLayer(
    // Use the same `TileLayer` as in the displaying map, but omit the `tileProvider`
    urlTemplate: 'https://tile.openstreetmap.org/{z}/{x}/{y}.png',
    //urlTemplate:
    //    'https://tiles.stadiamaps.com/tiles/stamen_toner/{z}/{x}/{y}.png',
    userAgentPackageName: 'com.example.app',
  ),
  // Additional parameters if necessary
);

//final store = FlutterMapTileCaching.instance('storeName');
//await store.manage.create(); // Create the store if necessary