import 'dart:convert';
import 'package:flutter/material.dart';

import 'package:flutter_map/flutter_map.dart';
import 'package:latlong2/latlong.dart';
import 'package:http/http.dart' as http;
import 'package:path_provider/path_provider.dart';
import 'dart:io';
import 'package:permission_handler/permission_handler.dart';
import 'package:file_manager/file_manager.dart';
import 'package:gpx/gpx.dart';
//import 'package:file_picker/file_picker.dart';

class Track {
  late String track;
  var trackList = <LatLng>[];

  Track(track) {
    if (track != null) {
      Iterable<String> list = LineSplitter.split(track);
      for (var e in list) {
        if (e.contains("<trkpt")) {
          List<String> cols = e.split("\"");
          //print(cols[1] + cols[3]);
          trackList.add(LatLng(double.parse(cols[1]), double.parse(cols[3])));
        }
      }
    }
    //print(trackList);
  }

  factory Track.fromString(String mystring) {
    return Track(mystring);
  }
}

Future<String> get _localPath async {
  var status = await Permission.storage.status;
  if (!status.isGranted) {
    await Permission.storage.request();
  }
  final directory = await getExternalStorageDirectory();
  return directory!.path;
}

Future<Track>? fetchTrack(courseName) async {
  if (courseName == null) {
    print("returning empty track!!!");
    return Track(courseName);
  }
  final file = File(courseName.path);
  String contents = await file.readAsString();
  return Track.fromString(contents);
}
