import 'package:flutter/material.dart';
import '../pages/map_ant.dart';
import '../pages/course_selection.dart';


class HomePage extends StatefulWidget {
  static const String route = '/';

  const HomePage({super.key});

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final titles = ['Select Gpx track', 'Just Show the map'];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Trailmap"),
      ),
      body: ListView.builder(
        itemCount: titles.length,
        itemBuilder: (BuildContext context, int index) {
          var post = titles[index];

          return Container(
              child: Card(
            child: ListTile(
              title: Text(post),
              onTap: () => onTapped(post, context),
            ),
          ));
        },
      ),
    );
  }

  void onTapped(String post, BuildContext context) {
    // navigate to the next screen.
    if (post == titles[0]) {
      Navigator.pushReplacementNamed(context, CourseSelection.route);
    } else {
      Navigator.pushReplacementNamed(context, CustomCrsPage.route);
    }
  }
}
