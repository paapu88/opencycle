import 'package:flutter/material.dart';
//import 'package:flutter_map/flutter_map.dart';
//import 'package:latlong2/latlong.dart';
//import 'package:http/http.dart' as http;
//import 'package:file_manager/file_manager.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:io';
import 'package:permission_handler/permission_handler.dart';
import '../pages/map_ant.dart';

class Courses {
  List<FileSystemEntity> tracks;

  //var courses = List<FileSystemEntity>;
  var courses = <FileSystemEntity>[];
  Courses({required this.tracks}) {
    for (final track in tracks) {
      courses.add(track);
    }
  }
  factory Courses.fromJson(List<FileSystemEntity> cs) {
    return Courses(tracks: cs);
  }
}

Future<String> get _localPath async {
  var status = await Permission.storage.status;
  if (!status.isGranted) {
    await Permission.storage.request();
  }
  final directory = await getExternalStorageDirectory();
  if (directory != null) {
    return directory.path;
  }
  return "null";
}

Future<Courses> fetchCourses() async {
  var status = await Permission.storage.status;
  if (!status.isGranted) {
    await Permission.storage.request();
  }
  //final path = await _localPath;
  //print(path);
  //var root = await getExternalStorageDirectory();
  var root = Directory('/sdcard/download/');
  var courses = root.listSync().where((e) => e is File);
  var coursesOk = <FileSystemEntity>[];
  for (final course in courses) {
    if (course.path.endsWith('gpx')) {
      coursesOk.add(course);
    }
  }
  // sort alphabetically, date only available for File
  coursesOk
      .sort((a, b) => a.path.toLowerCase().compareTo(b.path.toLowerCase()));
  return Courses.fromJson(coursesOk);
}

class CourseSelection extends StatelessWidget {
  static const String route = 'CourseSelection';

  const CourseSelection({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Valitse MTB-gpx reitti!"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: FutureBuilder<Courses>(
          future: fetchCourses(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return Column(
                children: <Widget>[
                  Expanded(
                    child: ListView.builder(
                      itemCount: snapshot.data!.courses.length,
                      itemBuilder: (BuildContext context, int index) {
                        var post = snapshot.data!.courses[index];

                        return Container(
                            child: Card(
                          child: ListTile(
                            title: Text(post.path.split("/").last),
                            onTap: () => onTapped(post, context),
                          ),
                        ));
                      },
                    ),
                  ),
                ],
              );
            } else if (snapshot.hasError) {
              return Text("${snapshot.error}");
            }

            // By default, show a loading spinner.
            return const CircularProgressIndicator();
          },
        ),
      ),
    );
  }

  void onTapped(FileSystemEntity post, BuildContext context) {
    // navigate to the next screen.
    Navigator.pushReplacementNamed(context, CustomCrsPage.route,
        arguments: post);
  }
}
