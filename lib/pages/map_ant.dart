import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_map/plugin_api.dart';
import 'package:geolocator/geolocator.dart';
import 'dart:async';
import 'package:geodesy/geodesy.dart';
import '../classes/piste.dart';
import 'dart:io';
import 'package:map_controller_plus/map_controller_plus.dart';
import 'package:flutter_map_tile_caching/flutter_map_tile_caching.dart';

import '../classes/track.dart';

class CustomCrsPage extends StatefulWidget {
  late FileSystemEntity? courseName;
  //CustomCrsPage({this.courseName});
  static const String route = 'custom_crs';

  CustomCrsPage({super.key});

  @override
  _CustomCrsPageState createState() => _CustomCrsPageState();
}

class _CustomCrsPageState extends State<CustomCrsPage> {
  late Position _position;
  LatLng oldp = LatLng(48.32, 14.31);
  LatLng newp = LatLng(48.32, 14.31);
  Piste piste = Piste(48.32, 14.31);
  bool found = false;
  double oldRotation = 0.0;
  double rotation = 0.0;
  late double pseudoRotation;
  double latitude = 48.32;
  double longitude = 14.31;
  Position? oldPosition = null;
  bool doRotate = true;
  Geodesy geodesy = Geodesy();
  double maxZoom = 17;
  String initText = 'Map centered to';
  late final MapController mapController;
  late final StatefulMapController statefulMapController;
  late final StreamSubscription<StatefulMapControllerStateChange> sub;
  bool ready = false;
  bool doCenter = true;
  late final Future<Track>? myFuture;
  //late final Future<List<Polyline>>? myFuture;

  void requestLocationPermissionAndInitPos() async {
    print("checking permissions");
    LocationPermission permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied ||
        permission == LocationPermission.deniedForever) {
      // Permissions are denied or denied forever, let's request it!
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        print("Location permissions are still denied");
      } else if (permission == LocationPermission.deniedForever) {
        print("Location permissions are permanently denied");
      } else {
        // Permissions are granted (either can be whileInUse, always, restricted).
        print("Location permissions are granted after requesting");
      }
    }
    //Position position = await Geolocator.getCurrentPosition(
    //  desiredAccuracy: LocationAccuracy.low,
    //);
    //setState(() {
    //  mapController.move(
    //    LatLng(position.latitude, position.longitude),
    //    15.0,
    //  );
    //});
  }

  @override
  void initState() {
    mapController = MapController();
    statefulMapController = StatefulMapController(mapController: mapController);
    sub = statefulMapController.changeFeed.listen((change) => setState(() {}));
    requestLocationPermissionAndInitPos();

    super.initState();
    const LocationSettings locationSettings = LocationSettings(
      accuracy: LocationAccuracy.bestForNavigation,
      distanceFilter: 1, //meters
    );

    StreamSubscription positionStream =
        Geolocator.getPositionStream(locationSettings: locationSettings)
            .listen((Position position) {
      //try {

      //} catch (e) {
      //  print("zoom problem!");
      //}
      print("getting location");
      setState(() {
        if (oldPosition == null) {
          oldPosition = position;
        } else {
          oldPosition = _position;
        }
        _position = position;
        latitude = _position.latitude;
        longitude = _position.longitude;
        print("latitude");
        print(latitude);
        print("longitude");
        print(longitude);
        print(doCenter);
        print(doRotate);
        piste = Piste(_position.latitude, _position.longitude);
        try {
          if (doCenter) {
            if (doRotate) {
              do_rotation();
              print("rotating");
              //mapController.move(
              //    LatLng(latitude, longitude), mapController.zoom);
            } else {
              mapController.move(
                  LatLng(latitude, longitude), mapController.zoom);
            }
          }
        } catch (e) {
          print("problem with mabcontroller move!!");
        }
        //do_rotation();
        //mapController.onRotationChanged(10.0);
      });
    });
  }

  void do_rotation() {
    //print("rotate1");
    if (oldPosition != null) {
      oldp = LatLng(oldPosition!.latitude, oldPosition!.longitude);
      newp = LatLng(_position.latitude, _position.longitude);
      rotation = geodesy.bearingBetweenTwoGeoPoints(oldp, newp).toDouble();
      if (rotation != oldRotation) {
        mapController.moveAndRotate(LatLng(latitude, longitude),
            mapController.zoom, (360.0 - rotation));
        oldRotation = rotation;
      }
    }
  }

  LatLng get_point() {
    if (doCenter) {
      if (oldPosition != null && doRotate) {
        print("rotating normally");
        do_rotation();
        print("did rotate ok");
      }
      print("moving to center before");
      print(latitude);
      print(longitude);
      mapController.move(LatLng(latitude, longitude), 14);
      print("moving to center after");
    }
    return LatLng(latitude, longitude);
  }

  Widget _darkModeTileBuilder(
    BuildContext context,
    Widget tileWidget,
    TileImage tile,
  ) {
    return ColorFiltered(
      colorFilter: const ColorFilter.matrix(<double>[
        //0.2126, 0.7152, 0.0722, 0, 0,
        //0.2126, 0.7152, 0.0722, 0, 0,
        //0.2126, 0.7152, 0.0722, 0, 0,
        //0, 0, 0, 1, 0,
        //-0.21, 0, 0, 0, 255, // Red channel
        //0, -0.71, 0, 0, 255, // Green channel
        //0, 0, -0.07, 0, 255, // Blue channel
        -0.2126, -0.7152, -0.0722, 0, 255, // red channel
        -0.2126, -0.7152, -0.0722, 0, 255, // green channel
        -0.2126, -0.7152, -0.0722, 0, 255, // Blue channel
        0, 0, 0, 1, 0, // Alpha channel
      ]),
      child: tileWidget,
    );
  }

  static ColorFilter brightnessAdjust(double value) {
    if (value >= 0.0) {
      value = value * 255;
    } else {
      value = value * 100;
    }
    List<double> matrix;
    matrix = [
      1,
      0,
      0,
      0,
      value,
      0,
      1,
      0,
      0,
      value,
      0,
      0,
      1,
      0,
      value,
      0,
      0,
      0,
      1,
      0
    ];
    return ColorFilter.matrix(matrix);
  }

  Widget _bw(
    BuildContext context,
    Widget tileWidget,
    TileImage tile,
  ) {
    return ColorFiltered(
      colorFilter: brightnessAdjust(-1),
      child: tileWidget,
    );
  }

  @override
  Widget build(BuildContext context) {
    var args;
    try {
      if (ModalRoute.of(context)!.settings.arguments != null) {
        args = ModalRoute.of(context)!.settings.arguments as FileSystemEntity;
      }
      myFuture = fetchTrack(args);
    } catch (e) {
      args = null;
      print("args problem");
    }
    //requestLocationPermissionAndInitPos();
    return Scaffold(
      //appBar: AppBar(title: Text('Custom CRS')),
      //drawer: buildDrawer(context, CustomCrsPage.route),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: FutureBuilder<Track?>(
          //child: FutureBuilder<List<Polyline>?>(
          future: myFuture,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              print("snapshot");
              print(snapshot.data!.trackList);
              return FlutterMap(
                mapController: mapController,
                options: MapOptions(
                    zoom: maxZoom - 2,
                    maxZoom: maxZoom,
                    onTap: (tapPos, LatLng latLng) {
                      const snackBar = SnackBar(
                        content: Text(
                            "PAINA PITKÄÄN niin vaihtuu: keskitetty/ei keskitetty kartta "),
                        duration: Duration(seconds: 3),
                        backgroundColor: Colors.red,
                      );
                      ScaffoldMessenger.of(context).showSnackBar(snackBar);
                    },
                    onLongPress: (tapPos, LatLng latLng) {
                      String teksti;
                      doCenter = !doCenter;
                      doRotate = !doRotate;
                      //print("docenter:" + doCenter.toString());
                      //print("dorotate:" + doCenter.toString());
                      if (doRotate) {
                        teksti = "Valittu: Kartan keskitys+pyöritys";
                      } else {
                        teksti = "Valittu: Kartan liikuttelu+skaalaus";
                      }
                      final snackBar = SnackBar(
                        content: Text(teksti),
                        duration: const Duration(seconds: 3),
                        backgroundColor: Colors.blue,
                      );
                      ScaffoldMessenger.of(context).showSnackBar(snackBar);
                      // get_point();
                    }),
                children: [
                  TileLayer(
                    urlTemplate:
                        //'https://{s}.tile-cyclosm.openstreetmap.fr/cyclosm-lite/{z}/{x}/{y}.png',
                        'https://tile.openstreetmap.org/{z}/{x}/{y}.png',
                    //'https://tiles.stadiamaps.com/tiles/stamen_toner/{z}/{x}/{y}.png',
                    userAgentPackageName: 'com.example.app',
                    tileProvider: FMTC.instance('mapStore').getTileProvider(),
                    tileBuilder: _bw,
                  ),
                  CircleLayer(
                      //circles: snapshot.data.controls + piste.piste),
                      circles: piste.piste),
                  PolylineLayer(polylines: [
                    Polyline(
                      points: snapshot.data!.trackList,
                      // isDotted: true,
                      color: Colors.red,
                      strokeWidth: 3.0,
                      borderColor: Colors.red,
                      //borderStrokeWidth: 0.1,
                    )
                  ])

                  //}
                ],
              );
            } else if (snapshot.hasError) {
              return Text("${snapshot.error}");
            }

            // By default, show a loading spinner.
            return const CircularProgressIndicator();
          },
        ),
      ),
    );
  }
}
